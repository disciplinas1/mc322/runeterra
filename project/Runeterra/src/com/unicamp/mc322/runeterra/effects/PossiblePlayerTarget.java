package com.unicamp.mc322.runeterra.effects;

public enum PossiblePlayerTarget {
	SELF("player"),
	ENEMY("enemy");

	String label;

	private PossiblePlayerTarget(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return this.label;
	}
}
