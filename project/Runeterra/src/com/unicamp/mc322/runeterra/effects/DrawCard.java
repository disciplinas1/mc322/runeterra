package com.unicamp.mc322.runeterra.effects;

import com.unicamp.mc322.runeterra.game.Player;

public class DrawCard extends EffectOnPlayer {

	// Constructor
	public DrawCard(PossiblePlayerTarget type) {
		super(type);
	}

	// Methods
	@Override
	void applyEffectOnGivenPlayer(Player player) {
		player.drawCard();
	}

	public String getLongDescription() {
		return "Make the " + this.type + " draw a card";
	}

}
