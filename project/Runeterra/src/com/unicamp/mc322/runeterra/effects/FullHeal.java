package com.unicamp.mc322.runeterra.effects;

import com.unicamp.mc322.runeterra.card.Unit;

public class FullHeal extends EffectOnUnity {

	// Constructor
    public FullHeal(PossibleUnitTargetType targetType) {
		super(targetType);
	}

	// Methods
    protected void applyEffectSingleUnit(Unit unit) {
    	unit.heal(unit.getCurrentHealth());
    }

	public String getLongDescription() {
		return "Fully heal " + this.targetType;
	}

}
