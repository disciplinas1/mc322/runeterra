package com.unicamp.mc322.runeterra.effects;

import java.util.ArrayList;

import com.unicamp.mc322.runeterra.card.Unit;

public class UnitAttackAllEnemyUnits extends EffectOnUnity {

	// Constructor
	public UnitAttackAllEnemyUnits(PossibleUnitTargetType targetType) {
		super(targetType);
	}

	// Methods
	@Override
	protected void applyEffectSingleUnit(Unit unit) {
		ArrayList<Unit> enemys = unit.getEnemyUnits();
		for (Unit enemy: enemys) {
			unit.attack(enemy);
		}
	}

	public String getLongDescription() {
		return "Make " + this.targetType + " attack all enemy units";
	}

}
