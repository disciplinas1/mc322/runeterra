package com.unicamp.mc322.runeterra.effects;

import java.util.ArrayList;

import com.unicamp.mc322.runeterra.card.Card;
import com.unicamp.mc322.runeterra.card.Unit;
import com.unicamp.mc322.runeterra.game.Board;

public abstract class EffectOnUnity extends Effect {

	// Properties
	protected PossibleUnitTargetType targetType;

	// Constructor
	public EffectOnUnity(PossibleUnitTargetType targetType) {
		this.targetType = targetType;
	}

	// Methods
	protected abstract void applyEffectSingleUnit(Unit unit);

	private void applyEffectOnCollection(ArrayList<Unit> units) {
		for (Unit unit: units) {
			this.applyEffectSingleUnit(unit);
		}
	}

    public final void applyEffect(Board board, Card card) {
		switch (targetType) {
			case ALL_ENEMY_UNITS:
				applyEffectOnCollection(card.getEnemyUnits());
				break;
			case ALL_OWN_UNITS:
				applyEffectOnCollection(card.getAlliedUnits());
				break;
			case CHOSEN:
				Unit target = this.chooseArbitraryUnitForEffect(card, board);
				if (target != null) {
					applyEffectSingleUnit(target);
				}

				break;
			case SELF:
				applyEffectSingleUnit((Unit) card);
				break;
			default:
				break;
		}
    }

}
