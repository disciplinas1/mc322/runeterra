package com.unicamp.mc322.runeterra.effects;

public enum PossibleUnitTargetType {
	ALL_OWN_UNITS("all your units"),
	ALL_ENEMY_UNITS("all enemy units"),
	CHOSEN("a chosen unit"),
	SELF("this unit");

	private String label;

	private PossibleUnitTargetType(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return this.label;
	}
}
