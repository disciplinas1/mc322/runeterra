package com.unicamp.mc322.runeterra.effects;

import java.util.ArrayList;
import java.util.Random;

import com.unicamp.mc322.runeterra.card.Card;
import com.unicamp.mc322.runeterra.card.Unit;
import com.unicamp.mc322.runeterra.game.*;

public abstract class Effect implements Describable {

    // Methods
    public abstract void applyEffect(Board board, Card card);

    // The methods below may return null in case there is no options available.
    protected Unit chooseAlliedUnitForEffect(Card card) {
		ArrayList<Unit> units = card.getAlliedUnits();
		System.out.println("Choose unit for effect");
		Unit chosenUnit = card.getPlayer().chooseOneOption(units);
        return chosenUnit;
    }

    protected Unit chooseEnemyUnitForEffect(Card card) {
		ArrayList<Unit> units = card.getEnemyUnits();
		System.out.println("Choose enemy unit for effect");
		Unit chosenUnit = card.getPlayer().chooseOneOption(units);
        return chosenUnit;
    }

    protected Unit chooseArbitraryUnitForEffect(Card card, Board board) {
		ArrayList<Unit> units = board.getAllEvokedUnits();
		System.out.println("Choose a unit for effect");
		Unit chosenUnit = card.getPlayer().chooseOneOption(units);
        return chosenUnit;
    }

    protected Unit getRandomAlly(Board board) {
        PlayerBoard playerBoard = board.getActivePlayerBoard();
		ArrayList<Unit> units = playerBoard.getAllUnits();
        Random random = new Random();
        int index = random.nextInt(units.size());
        Unit unit = units.get(index);
        return unit;
    }

}
