package com.unicamp.mc322.runeterra.effects;

import com.unicamp.mc322.runeterra.card.Card;
import com.unicamp.mc322.runeterra.game.Board;
import com.unicamp.mc322.runeterra.game.Player;

public abstract class EffectOnPlayer extends Effect {

	// Properties
	protected PossiblePlayerTarget type;

	// Constructor
	protected EffectOnPlayer(PossiblePlayerTarget type) {
		this.type = type;
	}

	// Methods
	abstract void applyEffectOnGivenPlayer(Player player);

	@Override
	public void applyEffect(Board board, Card card) {
		Player target;
		if (type == PossiblePlayerTarget.ENEMY) {
			target = card.getEnemyPlayer();
		} else {
			target = card.getPlayer();
		}
		applyEffectOnGivenPlayer(target);

	}

}
