package com.unicamp.mc322.runeterra.effects;

import com.unicamp.mc322.runeterra.card.Unit;

public class DoubleAttackAndHealth extends EffectOnUnity {

	// Constructor
    public DoubleAttackAndHealth(PossibleUnitTargetType targetType) {
		super(targetType);
	}

	// Methods
	@Override
	protected void applyEffectSingleUnit(Unit unit) {
        unit.addAttack(unit.getPermanentAttack());
        unit.addHealth(unit.getMaxHealth());
	}

	public String getLongDescription() {
		return "Double " + this.targetType + " attack and health";
	}

}
