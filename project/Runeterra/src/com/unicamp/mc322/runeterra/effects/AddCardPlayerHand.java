package com.unicamp.mc322.runeterra.effects;


import com.unicamp.mc322.runeterra.card.Card;
import com.unicamp.mc322.runeterra.deck.DeckLoader;
import com.unicamp.mc322.runeterra.game.Player;

public class AddCardPlayerHand extends EffectOnPlayer {

	// Properties
	private String cardName;

	// Constructor
	public AddCardPlayerHand(PossiblePlayerTarget type, String cardName) {
		super(type);
		this.cardName = cardName;
	}

	// Methods
	@Override
	void applyEffectOnGivenPlayer(Player player) {
		Card card = DeckLoader.getInstance().getCard(cardName);
		player.addCardToHand(card);
	}

	public String getLongDescription() {
		return "Add a " + this.cardName + " to " + this.type + " hand";
	}

}
