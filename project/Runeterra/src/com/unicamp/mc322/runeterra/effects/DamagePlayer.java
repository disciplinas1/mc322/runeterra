package com.unicamp.mc322.runeterra.effects;

import com.unicamp.mc322.runeterra.game.Player;

public class DamagePlayer extends EffectOnPlayer {

	// Properties
	private int damage;

	// Constructor
	public DamagePlayer(PossiblePlayerTarget type, int damage) {
		super(type);
		this.damage = damage;
	}

	// Methods
	@Override
	void applyEffectOnGivenPlayer(Player player) {
		player.takeDamage(damage);
	}

	public String getLongDescription() {
		return "Deal " + this.damage + " damage to the " + this.type;
	}

}
