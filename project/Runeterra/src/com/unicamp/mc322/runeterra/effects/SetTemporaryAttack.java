package com.unicamp.mc322.runeterra.effects;

import com.unicamp.mc322.runeterra.card.Unit;

public class SetTemporaryAttack extends EffectOnUnity {

	// Properties
	private int attackValue;

	// Constructor
	public SetTemporaryAttack(PossibleUnitTargetType targetType, int attackValue) {
		super(targetType);
		this.attackValue = attackValue;
	}

	// Methods
	@Override
	protected void applyEffectSingleUnit(Unit unit) {
		unit.addTemporaryAttack(attackValue); // Makes unit damage 0 until the end of the turn
	}

	public String getLongDescription() {
		return "Give " + this.targetType + " " + this.attackValue + " attack for this round";
	}

}
