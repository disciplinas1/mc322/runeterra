package com.unicamp.mc322.runeterra.effects;

import com.unicamp.mc322.runeterra.card.Unit;

public class UnitAttackEnemyNexus extends EffectOnUnity {

	// Constructor
	public UnitAttackEnemyNexus(PossibleUnitTargetType targetType) {
		super(targetType);
	}

	// Methods
	@Override
	protected void applyEffectSingleUnit(Unit unit) {
		unit.attack(unit.getEnemyPlayer());
	}

	public String getLongDescription() {
		return "Make " + this.targetType + " attack enemy player";
	}

}
