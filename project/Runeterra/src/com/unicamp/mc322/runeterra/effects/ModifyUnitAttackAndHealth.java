package com.unicamp.mc322.runeterra.effects;

import com.unicamp.mc322.runeterra.card.Unit;

public class ModifyUnitAttackAndHealth extends EffectOnUnity {

	// Properties
	private int attack;
	private int health;

	// Constructor
	public ModifyUnitAttackAndHealth(PossibleUnitTargetType targetType, int attack, int health) {
		super(targetType);
		this.attack = attack;
		this.health = health;
	}

	// Methods
	@Override
	public void applyEffectSingleUnit(Unit unit) {
		unit.addAttack(this.attack);
		unit.addHealth(this.health);
	}

	public String getLongDescription() {
		return "Give " + this.targetType + " " + this.attack + " attack and " + this.health + " health";
	}

}
