package com.unicamp.mc322.runeterra.effects;

import com.unicamp.mc322.runeterra.card.Unit;

public class GiveBarrier extends EffectOnUnity {

	// Properties
	private int barrierValue;

	// Constructor
	public GiveBarrier(PossibleUnitTargetType targetType, int barrierValue) {
		super(targetType);
		this.barrierValue = barrierValue;
	}

	// Methods
	@Override
	protected void applyEffectSingleUnit(Unit unit) {
		unit.addBarrier(barrierValue);
	}

	public String getLongDescription() {
		return "Give a barrier of " + this.barrierValue + " for " + this.targetType;
	}

}
