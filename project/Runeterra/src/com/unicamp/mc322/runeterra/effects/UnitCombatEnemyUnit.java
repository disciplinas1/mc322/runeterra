package com.unicamp.mc322.runeterra.effects;

import com.unicamp.mc322.runeterra.card.Unit;

public class UnitCombatEnemyUnit extends EffectOnUnity {

	// Constructor
	public UnitCombatEnemyUnit(PossibleUnitTargetType targetType) {
		super(targetType);
	}

	// Methods
	@Override
	protected void applyEffectSingleUnit(Unit unit) {
		Unit enemy = this.chooseEnemyUnitForEffect(unit);
		if (enemy != null) {
			unit.combatWith(enemy);
		}
	}

	public String getLongDescription() {
		return "Make " + this.targetType + " attack an enemy unit";
	}

}
