package com.unicamp.mc322.runeterra.utils;

public class PairContainer<A, B> {

	private A first;
	private B second;

	public PairContainer(A first, B second) {
		this.second = second;
		this.first = first;
	}

	public A getFirst() {
		return this.first;
	}

	public B getSecond() {
		return this.second;
	}

}
