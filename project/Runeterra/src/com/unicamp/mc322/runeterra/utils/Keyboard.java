package com.unicamp.mc322.runeterra.utils;

import java.util.ArrayList;
import java.util.Scanner;

public class Keyboard {

    // Static Properties
    private static final Scanner scanner = new Scanner(System.in);

    // Static Methods
    public static <T> ArrayList<T> chooseMultipleOptions(ArrayList<T> options) {
        Keyboard.printOptionsWithIndexes(options);
        int choice = 0;
        ArrayList<T> choices = new ArrayList<T>();
        System.out.println("Choose multiple indexes: (-1 to end selection)");

        while (choice >= 0 && choice < options.size()) {
            System.out.println("One of the indexes:");
            choice = Keyboard.chooseIntInInterval(-1, options.size() - 1);

            if (choice != -1) {
                T option = options.get(choice);
                if (!choices.contains(option)) {
                    choices.add(option);
                }
            }
        }

        return choices;
    }

    public static <T> T chooseOneOption(ArrayList<T> options) {
        Keyboard.printOptionsWithIndexes(options);
        System.out.println("Choose one index:");
        int choice = Keyboard.chooseIntInRange(options.size());
        return options.get(choice);
    }

    private static int chooseIntInRange(int range) {
        return Keyboard.chooseIntInInterval(0, range - 1);
    }

    private static int chooseIntInInterval(int start, int end) {
        int choice = start - 1;
        while (choice < start || choice > end) {
            choice = Keyboard.scanner.nextInt();
        }

        return choice;
    }

    private static <T> void printOptionsWithIndexes(ArrayList<T> options) {
        System.out.println("Available options:");

        for (int index = 0; index < options.size(); index++) {
            System.out.println(index + " - " + options.get(index));
        }
    }

}
