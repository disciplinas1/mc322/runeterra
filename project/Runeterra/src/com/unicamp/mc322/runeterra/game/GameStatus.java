package com.unicamp.mc322.runeterra.game;

public enum GameStatus {
    ROUND_IN_PROGRESS, ROUND_ENDED, DRAW, PLAYER1_WIN, PLAYER2_WIN;
}
