package com.unicamp.mc322.runeterra.game;

import java.util.ArrayList;
import com.unicamp.mc322.runeterra.utils.Keyboard;

public class HumanPlayer extends Player {

    // Constructor
    HumanPlayer(String name) {
        super(name);
    }

    // Methods
    @Override
    public <T> T chooseOneOption(ArrayList<T> options) {
        if (options.isEmpty()) {
            System.out.println("No options to choose");
            return null;
        }

        System.out.println(this.getName() + " to choose");
        return Keyboard.chooseOneOption(options);
    }

    @Override
    public <T> ArrayList<T> chooseMultipleOptions(ArrayList<T> options) {
        if (options.isEmpty()) {
            System.out.println("No options to choose");
            return null;
        }

        System.out.println(this.getName() + " to choose");
        return Keyboard.chooseMultipleOptions(options);
    }

    @Override
    public ArrayList<PlayOption> getPossiblePlays(Board board) {
        ArrayList<PlayOption> options = super.getPossiblePlays(board);
        options.add(PlayOption.RESIGN);
        return options;
    }

}
