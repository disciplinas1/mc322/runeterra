package com.unicamp.mc322.runeterra.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

import com.unicamp.mc322.runeterra.card.Card;
import com.unicamp.mc322.runeterra.card.Unit;
import com.unicamp.mc322.runeterra.utils.Keyboard;

public class Game {

    // Static Properties
    private static final String player1Name = "Player 1";
    private static final String player2Name = "Player 2";

    // Properties
    private Board board;
    private int roundCount;
    private GameStatus status;
    private boolean gameEnded;

    // Constructor
    public Game() {
        this.board = this.chooseGameModeAndCreateBoard();
        this.board.setDecks("Demacia", "Demacia");
        this.roundCount = 0;
        this.status = GameStatus.ROUND_IN_PROGRESS;
        this.gameEnded = false;
    }

    // Methods
    public void play() {
        this.board.makePlayersDrawInitialCards();

        while (!gameEnded) {
            this.status = GameStatus.ROUND_IN_PROGRESS;
            this.roundCount += 1;
            this.board.toggleRound(this.roundCount);

            while (this.status == GameStatus.ROUND_IN_PROGRESS) {
                this.drawCurrentPlayerBoard();
                this.status = this.board.makeActivePlayerPlay();
                switch (this.status) {
                    case DRAW:
                    case PLAYER1_WIN:
                    case PLAYER2_WIN:
                        this.gameEnded = true;
                        break;

                    default:
                        break;
                }

                this.board.toggleTurn();
            }
        }

        this.printEndGame();
    }

    private void printEndGame() {
        String message = "";
        switch (this.status) {
            case DRAW:
                message = "Draw";
                break;
            case PLAYER1_WIN:
                message = player1Name + " Wins!!";
                break;
            case PLAYER2_WIN:
                message = player2Name + " Wins!!";
                break;
            default:
                throw new IllegalStateException("Game ended without valid status: " + this.status);
        }

        System.out.println("Game over!");
        System.out.println(message);
    }

    private void drawAdversaryInformation(Scanner sc, Board board) {
        PlayerBoard adversaryBoard = board.getInactivePlayerBoard();
        Player adversary = board.getPlayers().getInactivePlayer();
        System.out.printf("\r\n" + sc.next(), adversary.getName() + "\r\n");
        System.out.printf(sc.next(), adversary.getCurrentHealth(), adversary.getDeckCurrentSize(), adversary.getMana()
        , adversary.getSpellMana());
        System.out.println("\r\n" + sc.next());
        ArrayList<Unit> evoked = adversaryBoard.getEvokedUnits();
        for (int i = 0; i < evoked.size(); i++) {
            System.out.print(i + " - ");
            System.out.println(evoked.get(i).getLongDescription());
        }
        if (board.isPlayerAttacker(adversary)) {
            System.out.println("\r\nAttack:");
        } else {
            System.out.println("\r\nDefence:");
        }
        Unit[] adversaryBattleField = adversaryBoard.getBattleField();
        for (int i = 0; i < PlayerBoard.maxEvokedUnits; i++) {
            if (adversaryBattleField[i] != null) {
                System.out.println(i + " - " + adversaryBattleField[i].getLongDescription());
            }
        }
    }

    private void drawCurrentPlayerInformation(Scanner sc, Board board) {
        PlayerBoard currentPlayerBoard = board.getActivePlayerBoard();
        Player currentPlayer = board.getPlayers().getActivePlayer();
        if (board.isPlayerAttacker(currentPlayer)) {
            System.out.println("\r\nAttack:\r\n");
        } else {
            System.out.println("\r\nDefence:\r\n");
        }
        Unit[] adversaryBattleField = currentPlayerBoard.getBattleField();
        for (int i = 0; i < PlayerBoard.maxEvokedUnits; i++) {
            if (adversaryBattleField[i] != null) {
                System.out.println(i + " - " + adversaryBattleField[i].getLongDescription());
            }
        }
        System.out.println(sc.next() + "\r\n");
        ArrayList<Unit> evoked = currentPlayerBoard.getEvokedUnits();
        for (int i = 0; i < evoked.size(); i++) {
            System.out.print(i + " - ");
            System.out.println(evoked.get(i).getLongDescription());
        }
        System.out.println(sc.next());
        ArrayList<Card> currentPlayerHand = currentPlayer.getHandCards();
        for (int i = 0; i < currentPlayerHand.size(); i++) {
            System.out.println(i + " - " + currentPlayerHand.get(i).getLongDescription());
        }
        System.out.printf(sc.next(), currentPlayer.getName() + "\r\n");
        System.out.printf(sc.next() + "\r\n", currentPlayer.getCurrentHealth(), currentPlayer.getDeckCurrentSize(), currentPlayer.getMana(), currentPlayer.getSpellMana());
    }

    private void drawCurrentPlayerBoard() {
        File file = new File("resources" + File.separator + "debug.txt");
        Scanner sc;
        try {
            sc = new Scanner(file);
            sc.useDelimiter("\n");
            System.out.printf(sc.next(), roundCount);
            drawAdversaryInformation(sc, board);
            drawCurrentPlayerInformation(sc, board);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Board chooseGameModeAndCreateBoard() {
        System.out.println("Choose a game mode:");
        ArrayList<GameMode> options = new ArrayList<GameMode>(Arrays.asList(GameMode.values()));
        GameMode choice = Keyboard.chooseOneOption(options);
        Player player1;
        Player player2;
        switch (choice) {
            case PVP:
                player1 = new HumanPlayer(player1Name);
                player2 = new HumanPlayer(player2Name);
                break;
            case PVB:
                player1 = new HumanPlayer(player1Name);
                player2 = new BotPlayer(player2Name);
                break;
            case BVB:
                player1 = new BotPlayer(player1Name);
                player2 = new BotPlayer(player2Name);
                break;
            default:
                throw new IllegalStateException("Illegal choice of game mode");
        }

        return new Board(player1, player2);
    }


}
