package com.unicamp.mc322.runeterra.game;

import java.util.ArrayList;

import com.unicamp.mc322.runeterra.card.Combatant;
import com.unicamp.mc322.runeterra.card.Unit;

public class PlayerBoard {

    // Static Properties
    public static final int maxEvokedUnits = 6;

    // Properties
    private Player player;
    private ArrayList<Unit> evokedUnits;
    private Unit[] battleField;

    // Constructor
    PlayerBoard(Player player) {
        this.player = player;
        this.evokedUnits = new ArrayList<Unit>();
        this.battleField = new Unit[maxEvokedUnits];
    }

    // Methods
    void applyEndRoundEffects() {
        for (Unit unit: this.getAllUnits()) {
            unit.activateEffectOnRoundEnd();
        }
    }

    public void addToEvokedUnits(Unit unit) {
        if (this.evokedUnits.size() == maxEvokedUnits) {
            throw new IllegalStateException("Cannot evoke unit with full board");
        }

        this.evokedUnits.add(unit);
    }

    public ArrayList<Unit> getAllUnits() {
        ArrayList<Unit> allUnits = new ArrayList<Unit>();
        allUnits.addAll(this.evokedUnits);
        for (Unit unit: this.battleField) {
            if (unit != null) {
                allUnits.add(unit);
            }
        }

        return allUnits;
    }

    public ArrayList<Unit> getEvokedUnits() {
        return this.evokedUnits;
    }

    public boolean isBoardOf(Player player) {
        return this.player == player;
    }

    void moveToBattleField(Unit unit) {
        for (int position = 0; position < maxEvokedUnits; position++) {
            if (this.battleField[position] == null) {
                this.moveToBattleField(unit, position);
                return;
            }
        }

        throw new IllegalStateException("Trying to move to battleField but it's full");
    }

    void moveToBattleField(Unit unit, int position) {
        if (!isInBattleFieldPositionRange(position)) {
            throw new IllegalArgumentException("Invalid position for battlefield: " + position);
        }

        if (this.battleField[position] != null) {
            throw new IllegalStateException("Cannot move to battleField position already occupied: " + position);
        }

        boolean didRemove = this.evokedUnits.remove(unit);
        if (!didRemove) {
            throw new IllegalArgumentException("Trying to move not evoked unit to battlefield");
        }

        this.battleField[position] = unit;
    }

    void removeAllUnitsFromBattleField() {
        for (int position = 0; position < maxEvokedUnits; position++) {
            Unit unit = this.battleField[position];
            if (unit != null) {
                this.battleField[position] = null;

                if (!unit.isDead()) {
                    this.addToEvokedUnits(unit);
                }
            }
        }
    }

    void makeBattleFieldUnitsAttack(PlayerBoard other) {
        for (int position = 0; position < maxEvokedUnits; position++) {
            Unit attacker = this.battleField[position];
            if (attacker != null) {
                Combatant target = other.battleField[position];
                if (target == null) {
                    target = other.player;
                }

                attacker.combatWith(target);
            }
        }
    }

    Unit[] getBattleField() {
        return this.battleField;
    }

    public boolean isEvokedUnitsEmpty() {
        return evokedUnits.size() == 0;
    }

    private static boolean isInBattleFieldPositionRange(int position) {
        return position >= 0 && position < maxEvokedUnits;
    }

}
