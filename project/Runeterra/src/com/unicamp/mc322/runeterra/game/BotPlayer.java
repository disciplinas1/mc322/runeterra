package com.unicamp.mc322.runeterra.game;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class BotPlayer extends Player {

    // Static Properties
    private static final int delay = 2;

    // Properties
    Random random;

    // Constructor
    BotPlayer(String name) {
        super(name);
        this.random = new Random();
    }

    // Methods
    @Override
    public <T> T chooseOneOption(ArrayList<T> options) {
        if (options.isEmpty()) {
            System.out.println("No options to choose");
            return null;
        }

        System.out.println(this.getName() + " to choose");
        int index = this.random.nextInt(options.size());
        T choice = options.get(index);

        this.waitDelay();
        System.out.println("Bot made its choice");
        this.waitDelay();
        return choice;
    }

    @Override
    public <T> ArrayList<T> chooseMultipleOptions(ArrayList<T> options) {
        if (options.isEmpty()) {
            System.out.println("No options to choose");
            return null;
        }

        System.out.println(this.getName() + " to choose");
        ArrayList<T> choices = new ArrayList<T>();
        for (T element: options) {
            if (this.random.nextBoolean()) {
                choices.add(element);
            }
        }

        this.waitDelay();
        System.out.println("Bot made its choices");
        this.waitDelay();
        return choices;
    }

    private void waitDelay() {
        try {
            TimeUnit.SECONDS.sleep(delay);
        } catch (InterruptedException e) {
            System.out.println("Bot delay failed, game moving on");
        }
    }

}
