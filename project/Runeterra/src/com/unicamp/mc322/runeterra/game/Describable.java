package com.unicamp.mc322.runeterra.game;

public interface Describable {

    public String getLongDescription();

}
