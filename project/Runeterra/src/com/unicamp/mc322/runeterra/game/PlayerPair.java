package com.unicamp.mc322.runeterra.game;

import com.unicamp.mc322.runeterra.deck.DeckLoader;

public class PlayerPair {

    // Static Properties
    private static final int first = 0;
    private static final int second = 1;

    // Properties
    private Player[] players;
    private int attacker;
    private int whoseTurn;

    // Constructor
    PlayerPair(Player player1, Player player2) {
        this.players = new Player[]{player1, player2};
        this.attacker = PlayerPair.second;
        this.whoseTurn = this.attacker;
    }

    // Methods
    void setDecks(String deck1Name, String deck2Name, DeckLoader deckLoader) {
        this.players[first].setDeck(deck1Name, deckLoader);
        this.players[second].setDeck(deck2Name, deckLoader);
    }

    void makePlayersDrawInitialCards() {
        for (Player player: this.players) {
            player.drawInitialCards();
        }
    }

    public Player getPlayer1() {
        return this.players[first];
    }

    public Player getPlayer2() {
        return this.players[second];
    }

    public Player getActivePlayer() {
        return this.players[this.whoseTurn];
    }

    public Player getInactivePlayer() {
        int inactivePlayerIndex = getOtherPlayerIndex(this.whoseTurn);
        return this.players[inactivePlayerIndex];
    }

    public Player getAttackerPlayer() {
        return this.players[this.attacker];
    }

    Player getDefenderPlayer() {
        return this.players[getOtherPlayerIndex(this.attacker)];
    }

    void toggleRound(int roundCount) {
        this.toggleAttacker();
        this.whoseTurn = this.attacker;
        this.setPlayersMana(roundCount);
        this.getAttackerPlayer().drawCard();
        this.getDefenderPlayer().drawCard();
    }

    private void setPlayersMana(int roundCount) {
        for (Player player: this.players) {
            player.setRoundMana(roundCount);
        }
    }

    void toggleTurn() {
        this.whoseTurn = getOtherPlayerIndex(this.whoseTurn);
    }

    Player[] getPlayers() {
        return this.players;
    }

    private void toggleAttacker() {
        this.attacker = getOtherPlayerIndex(this.attacker);
    }

    private static int getOtherPlayerIndex(int playerIndex) {
        int otherPlayerIndex = PlayerPair.first;

        switch (playerIndex) {
            case PlayerPair.first:
                otherPlayerIndex = PlayerPair.second;
                break;

            case PlayerPair.second:
                otherPlayerIndex = PlayerPair.first;
                break;
        }

        return otherPlayerIndex;
    }

    public Player getOtherPlayer(Player player) {
    	Player[] players = getPlayers();
    	if (player == players[0]) {
    		return players[1];
    	} else {
    		return players[0];
    	}
    }

}
