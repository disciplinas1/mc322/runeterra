package com.unicamp.mc322.runeterra.game;

public enum PlayOption {
    PLAY_CARD("Play a card"),
    ATTACK("Set attackers"),
    DEFEND("Set defenders"),
    PASS_TURN("Pass turn"),
    RESIGN("Resign game");

    private String label;

    private PlayOption(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return this.label;
    }
}
