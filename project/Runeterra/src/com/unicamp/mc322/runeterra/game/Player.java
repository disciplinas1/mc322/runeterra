package com.unicamp.mc322.runeterra.game;

import java.util.ArrayList;

import com.unicamp.mc322.runeterra.card.*;
import com.unicamp.mc322.runeterra.deck.*;

public abstract class Player implements Combatant {

    // Static Properties
    public static final int initialHealth = 20;
    public static final int maxMana = 10;
    public static final int maxSpellMana = 3;
    public static final int initialCards = 4;

    // Properties
    private String name;
    private ArrayList<Card> handCards;
    private int maxHealth;
    private int currentHealth;
    private int mana;
    private int spellMana;
    private Deck deck;

    // Constructor
    Player(String name) {
        this.name = name;
        this.handCards = new ArrayList<Card>();
        this.maxHealth = initialHealth;
        this.currentHealth = initialHealth;
        this.mana = 0;
        this.spellMana = 0;
    }

    // Methods
    void setDeck(String deckName, DeckLoader deckLoader) {
        this.deck = deckLoader.getDeck(deckName);
    }

    PlayOption playTurn(Board board) {
        ArrayList<PlayOption> options = this.getPossiblePlays(board);
        System.out.println("Choose your move:");
        PlayOption choice = this.chooseOneOption(options);

        switch (choice) {
            case PLAY_CARD:
                this.chooseAndPlayACard(board);
                break;
            case ATTACK:
                this.chooseUnitsToAttack(board);
                break;
            case DEFEND:
                this.chooseUnitsToDefend(board);
                break;
            default:
                break;
        }

        return choice;
    }

    private void chooseAndPlayACard(Board board) {
        ArrayList<Card> playableCards = this.getPlayableCards();
        System.out.println("Choose a card to play:");
        Card cardToPlay = this.chooseOneOption(playableCards);
        this.handCards.remove(cardToPlay);
        cardToPlay.play(this);
    }

    private void chooseUnitsToAttack(Board board) {
        board.startCombatMode();
        ArrayList<Unit> units = this.getEvokedUnits(board);
        System.out.println("Choose units to attack:");
        ArrayList<Unit> selectedUnits = this.chooseMultipleOptions(units);
        PlayerBoard playerBoard = board.getPlayerBoard(this);
        for (Unit unit: selectedUnits) {
            playerBoard.moveToBattleField(unit);
        }
    }

    private void chooseUnitsToDefend(Board board) {
        ArrayList<Unit> ownUnits = this.getEvokedUnits(board);
        PlayerBoard ownBoard = board.getPlayerBoard(this);
        Unit[] attackerPlayerBattleField = board.getAttackerPlayerBoard().getBattleField();
        for (int position = 0; position < PlayerBoard.maxEvokedUnits; position++) {
            Unit attackerUnit = attackerPlayerBattleField[position];
            if (attackerUnit != null) {
                ArrayList<Unit> options = getOptionsToDefend(ownUnits, attackerUnit);
                if (!options.isEmpty()) {
                    System.out.println("Choose a unit to battle with:");
                    System.out.println(attackerUnit);
                    Unit unit = this.chooseOneOption(options);
                    ownBoard.moveToBattleField(unit, position);
                    ownUnits = this.getEvokedUnits(board);
                }
            }
        }
    }

    private ArrayList<Unit> getOptionsToDefend(ArrayList<Unit> ownUnits, Unit attackerUnit) {
        ArrayList<Unit> options = new ArrayList<Unit>();
        for (Unit possibleOption: ownUnits) {
            if (possibleOption.canBlock(attackerUnit)) {
                options.add(possibleOption);
            }
        }

        return options;
    }

    private ArrayList<Unit> getEvokedUnits(Board board) {
        PlayerBoard playerBoard = board.getPlayerBoard(this);
        return playerBoard.getEvokedUnits();
    }

    protected ArrayList<PlayOption> getPossiblePlays(Board board) {
        ArrayList<PlayOption> options = new ArrayList<PlayOption>();
        boolean isAttacker = board.isPlayerAttacker(this);
        boolean combatModeDidHappen = board.happenedCombatMode();
        boolean combatModeIsHappening = board.isInCombatMode();
        boolean isOwnBoardEmpty = board.getPlayerBoard(this).isEvokedUnitsEmpty();

        //options.add(PlayOption.RESIGN);
        options.add(PlayOption.PASS_TURN);

        if (!isAttacker && combatModeIsHappening && !isOwnBoardEmpty) {
            options.add(PlayOption.DEFEND);
        } else {
            if (this.hasCardsToPlay()) {
                options.add(PlayOption.PLAY_CARD);
            }

            if (!combatModeIsHappening && isAttacker && !combatModeDidHappen && !isOwnBoardEmpty) {
                options.add(PlayOption.ATTACK);
            }
        }

        return options;
    }

    private boolean hasCardsToPlay() {
        for (Card card: this.handCards) {
            if (card.canBePlayed(this)) {
                return true;
            }
        }

        return false;
    }

    private ArrayList<Card> getPlayableCards() {
        ArrayList<Card> cards = new ArrayList<Card>();
        for (Card card: this.handCards) {
            if (card.canBePlayed(this)) {
                cards.add(card);
            }
        }

        return cards;
    }

    void drawInitialCards() {
        for (int cardCount = 0; cardCount < initialCards; cardCount++) {
            this.drawCard();
        }

        System.out.println("Choose cards to swap for new ones:");
        ArrayList<Card> cardsToSwap = this.chooseMultipleOptions(handCards);
        for (Card card: cardsToSwap) {
            this.handCards.remove(card);
            this.deck.addCard(card);
            this.drawCard();
        }
    }

    public void drawCard() {
        Card card = this.deck.drawCard(this);
        this.handCards.add(card);
    }

    public void addCardToHand(Card card) {
        this.handCards.add(card);
    }

    void setRoundMana(int roundCount) {
        int oldRemainingMana = this.mana + this.spellMana;
        this.spellMana = Math.min(oldRemainingMana, maxSpellMana);
        this.mana = Math.min(roundCount, maxMana);
    }

    public void spendMana(int value) {
        if (this.mana < value) {
            throw new IllegalStateException("Not enough mana");
        }

        this.mana -= value;
    }

    public void spendSpellMana(int value) {
        if (this.spellMana < value) {
            throw new IllegalStateException("Not enough spell mana");
        }

        this.spellMana -= value;
    }

    public int getMana() {
        return this.mana;
    }

    public int getSpellMana() {
        return this.spellMana;
    }

    public void heal(int value) {
		this.currentHealth = Math.min(this.currentHealth + value, this.maxHealth);
	}

    @Override
    public void takeDamage(int damage) {
        this.currentHealth -= damage;
    }

	@Override
	public void attack(Combatant other) {
		return;
	}

    public boolean isDead() {
        return this.currentHealth <= 0;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public int getDeckCurrentSize() {
        return deck.getSize();
    }

    ArrayList<Card> getHandCards() {
        return handCards;
    }

    public String getName() {
        return this.name;
    }

    // Abstract Methods
    public abstract <T> T chooseOneOption(ArrayList<T> options);

    public abstract <T> ArrayList<T> chooseMultipleOptions(ArrayList<T> options);

}
