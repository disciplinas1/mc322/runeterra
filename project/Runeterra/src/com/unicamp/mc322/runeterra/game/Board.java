package com.unicamp.mc322.runeterra.game;

import java.util.ArrayList;

import com.unicamp.mc322.runeterra.card.Unit;
import com.unicamp.mc322.runeterra.deck.DeckLoader;

public class Board {

    // Properties
    private PlayerBoard playerBoard1;
    private PlayerBoard playerBoard2;
    private PlayerPair players;
    private boolean combatModeDidHappen;
    private boolean combatModeIsHappening;
    private PlayOption lastPlay;

    // Constructor
    Board(Player player1, Player player2) {
        this.playerBoard1 = new PlayerBoard(player1);
        this.playerBoard2 = new PlayerBoard(player2);
        this.players = new PlayerPair(player1, player2);
        this.combatModeDidHappen = false;
        this.combatModeIsHappening = false;
        this.lastPlay = null;
    }

    // Methods
    void setDecks(String deck1Name, String deck2Name) {
        DeckLoader deckLoader = DeckLoader.getInstance();
        deckLoader.setBoard(this);
        this.players.setDecks(deck1Name, deck2Name, deckLoader);
    }

    GameStatus makeActivePlayerPlay() {
        Player activePlayer = this.players.getActivePlayer();
        PlayOption option = activePlayer.playTurn(this);
        if (option == PlayOption.RESIGN) {
            return getStatusAtResign(activePlayer);
        }

        if (this.combatModeIsHappening && !this.isPlayerAttacker(activePlayer)) {
            this.makeBattleFieldUnitsAttack();
            this.endCombatMode();
        }

        GameStatus status = getGameStatus(option);
        lastPlay = option;
        return status;
    }

    private GameStatus getStatusAtResign(Player activePlayer) {
        if (activePlayer == this.players.getPlayer1()) {
            return GameStatus.PLAYER2_WIN;
        } else {
            return GameStatus.PLAYER1_WIN;
        }
    }

    private GameStatus getGameStatus(PlayOption option) {
        GameStatus status = getGameStatusIfGameEnded();
        if (status != null) {
            return status;
        }

        switch (option) {
            case DEFEND:
                return GameStatus.ROUND_ENDED;

            case PASS_TURN:
                if (lastPlay == PlayOption.ATTACK || lastPlay == PlayOption.PASS_TURN) {
                    return GameStatus.ROUND_ENDED;
                } else {
                    return GameStatus.ROUND_IN_PROGRESS;
                }

            default:
                return GameStatus.ROUND_IN_PROGRESS;
        }
    }

    private GameStatus getGameStatusIfGameEnded() {
        boolean player1Dead = this.players.getPlayer1().isDead();
        boolean player2Dead = this.players.getPlayer2().isDead();
        if (player1Dead && player2Dead) {
            return GameStatus.DRAW;
        } else if (!player1Dead && player2Dead) {
            return GameStatus.PLAYER1_WIN;
        } else if (player1Dead && !player2Dead) {
            return GameStatus.PLAYER2_WIN;
        } else {
            return null;
        }
    }

    void startCombatMode() {
        this.combatModeIsHappening = true;
    }

    private void endCombatMode() {
        this.combatModeIsHappening = false;
        this.combatModeDidHappen = true;
        this.playerBoard1.removeAllUnitsFromBattleField();
        this.playerBoard2.removeAllUnitsFromBattleField();
    }

    void toggleTurn() {
        this.players.toggleTurn();
    }

    void makeBattleFieldUnitsAttack() {
        PlayerBoard attackerPlayerBoard = this.getAttackerPlayerBoard();
        PlayerBoard defenderPlayerBoard = this.getDefenderPlayerBoard();
        attackerPlayerBoard.makeBattleFieldUnitsAttack(defenderPlayerBoard);
    }

    void makePlayersDrawInitialCards() {
        this.players.makePlayersDrawInitialCards();
    }

    boolean isInCombatMode() {
        return this.combatModeIsHappening;
    }

    boolean happenedCombatMode() {
        return this.combatModeDidHappen;
    }

    public boolean isPlayerAttacker(Player player) {
        return this.players.getAttackerPlayer() == player;
    }

    void toggleRound(int roundCount) {
        this.players.toggleRound(roundCount);
        this.combatModeDidHappen = false;
        this.combatModeIsHappening = false;
        this.applyAllEndRoundEffects();
        this.lastPlay = null;
    }

    private void applyAllEndRoundEffects() {
        this.playerBoard1.applyEndRoundEffects();
        this.playerBoard2.applyEndRoundEffects();
    }

    public PlayerBoard getAttackerPlayerBoard() {
        Player attackerPlayer = this.players.getAttackerPlayer();
        return this.getPlayerBoard(attackerPlayer);
    }

    public PlayerBoard getDefenderPlayerBoard() {
        Player defenderPlayer = this.players.getDefenderPlayer();
        return this.getPlayerBoard(defenderPlayer);
    }

    public PlayerBoard getActivePlayerBoard() {
        Player activePlayer = this.players.getActivePlayer();
        return this.getPlayerBoard(activePlayer);
    }

    public PlayerBoard getInactivePlayerBoard() {
        Player inactivePlayer = this.players.getInactivePlayer();
        return this.getPlayerBoard(inactivePlayer);
    }

    public PlayerBoard getPlayerBoard(Player player) {
        if (playerBoard1.isBoardOf(player)) {
            return playerBoard1;
        } else {
            return playerBoard2;
        }
    }

    public Player getOtherPlayer(Player player) {
    	return players.getOtherPlayer(player);
    }

    public ArrayList<Unit> getPlayerEnemyUnits(Player player) {
    	Player enemy = getOtherPlayer(player);
    	return getPlayerBoard(enemy).getEvokedUnits();
    }

    public ArrayList<Unit> getPlayerEvokedUnits(Player player) {
    	return getPlayerBoard(player).getEvokedUnits();
    }

    public ArrayList<Unit> getAllEvokedUnits() {
    	ArrayList<Unit> result = new ArrayList<Unit>();
    	result.addAll( playerBoard1.getAllUnits() );
    	result.addAll( playerBoard2.getAllUnits() );
    	return result;
    }

    public PlayerPair getPlayers() {
        return this.players;
    }

}
