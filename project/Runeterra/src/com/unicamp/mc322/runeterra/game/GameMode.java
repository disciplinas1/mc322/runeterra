package com.unicamp.mc322.runeterra.game;

public enum GameMode {
    PVP("Human vs human"),
    PVB("Human vs bot"),
    BVB("Bot vs bot");

    String label;

    private GameMode(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return this.label;
    }

}
