package com.unicamp.mc322.runeterra.card;

public interface Combatant {

    void takeDamage(int damage);

    void attack(Combatant other);

    boolean isDead();

    default void combatWith(Combatant other) {
    	attack(other);
    	other.attack(this);
    }

}
