package com.unicamp.mc322.runeterra.card;

public enum LevelUpConditions {
	ATTACK_N_TIMES("attack", "times"),
	KILL_N_UNITS("kill", "units"),
	CAUSE_N_DAMAGE("cause", "damage"),
	INCR_N_DAMAGE("increase", "damage");

	String firstLabel;
	String secondLabel;

	private LevelUpConditions(String firstLabel, String secondLabel) {
		this.firstLabel = firstLabel;
		this.secondLabel = secondLabel;
	}

	public String getDescription(int value) {
		return this.firstLabel + " " + value + " " + this.secondLabel;
	}

}
