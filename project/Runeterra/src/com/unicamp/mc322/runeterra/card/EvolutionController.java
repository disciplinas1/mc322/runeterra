package com.unicamp.mc322.runeterra.card;

import java.util.ArrayList;
import com.unicamp.mc322.runeterra.effects.Effect;
import com.unicamp.mc322.runeterra.game.Describable;
import com.unicamp.mc322.runeterra.utils.PairContainer;

public class EvolutionController implements Describable {

	// Properties
	private int bonusAttack, bonusHealth;
	private ArrayList<Trait> traits;
	private ArrayList<PairContainer<Effect, UnitEffectTrigger>> effects;

	// Constructor
	public EvolutionController(int bonusAttack, int bonusHealth) {
		this.bonusHealth = bonusHealth;
		this.bonusAttack = bonusAttack;
		effects = new ArrayList< PairContainer< Effect, UnitEffectTrigger>>();
		traits = new ArrayList<Trait>();
	}

	// Methods
	public void addTrait(Trait trait) {
		traits.add(trait);
	}

	public void addTraits(Iterable<Trait> traits) {
		for (Trait trait: traits) {
			addTrait(trait);
		}
	}

	public void addEffect(UnitEffectTrigger trigger, Effect effect) {
		effects.add(new PairContainer< Effect, UnitEffectTrigger>( effect, trigger));
	}

	public void evolve(Champion champion) {
		champion.addAttack(this.bonusAttack);
		champion.addHealth(bonusHealth);
		champion.addTraits(traits);
		champion.addEffects(effects);
	}

	public String getLongDescription() {
		String description = "";
		boolean hasSomeDescription = false;

		// Bonus attack and health.
		if (bonusAttack != 0 || bonusHealth != 0) {
			if (hasSomeDescription) {
				description += ";";
			}

			description += " +" + bonusAttack + "/+" + bonusHealth;
			hasSomeDescription = true;
		}

		// Traits.
		if (!traits.isEmpty()) {
			if (hasSomeDescription) {
				description += ";";
			}

			description += " Receive:";
			for (Trait trait: traits) {
				description += " \"" + trait + "\"";
			}

			hasSomeDescription = true;
		}

		// Effects.
		if (!effects.isEmpty()) {
			if (hasSomeDescription) {
				description += ";";
			}

			description += getEffectString();
			hasSomeDescription = true;
		}

		description += ".";
		return description;
	}

	private String getEffectString() {
		String description = "";
		for (PairContainer<Effect, UnitEffectTrigger> pair: effects) {
			Effect effect = pair.getFirst();
			UnitEffectTrigger trigger = pair.getSecond();

			description += " " + trigger + ":";
			description += " " + effect.getLongDescription();

			if (effects.indexOf(pair) != effects.size() - 1) {
				description += ";";
			}
		}

		return description;
	}

}
