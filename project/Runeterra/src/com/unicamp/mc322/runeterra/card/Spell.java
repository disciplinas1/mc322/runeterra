package com.unicamp.mc322.runeterra.card;

import com.unicamp.mc322.runeterra.game.*;

import java.util.ArrayList;

import com.unicamp.mc322.runeterra.effects.Effect;

public class Spell extends Card {

	// Properties
	private ArrayList<Effect> effects;

	// Constructor
	public Spell(String name, int cost, Board board){
		super(name, cost, board);
		effects = new ArrayList<Effect>();
	}

	// Methods
	public void addEffect(Effect effect) {
		effects.add(effect);
	}

	@Override
	public void play(Player player) {
		int spellMana = player.getSpellMana();
		int mana = player.getMana();
		if (spellMana >= manaCost) {
			player.spendSpellMana(manaCost);
		} else {
			player.spendSpellMana(spellMana);
			player.spendMana(mana - spellMana);
		}

		for (Effect effect: effects) {
			effect.applyEffect(board, this);
		}
	}

	@Override
	public boolean canBePlayed(Player player) {
		return (player.getSpellMana() + player.getMana()) >= manaCost;
	}

	@Override
	public String getLongDescription() {
		String description = super.getLongDescription() + " Effect:";
		for (Effect effect: this.effects) {
			description += " " + effect.getLongDescription() + ".";
		}

		return description;
	}

}
