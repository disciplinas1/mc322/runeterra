package com.unicamp.mc322.runeterra.card;

import com.unicamp.mc322.runeterra.game.Board;

public class Champion extends Unit {

	// Properties
	private boolean hasLeveledUp;
	LevelUpConditions condition;
	private int conditionParameter;
	EvolutionController evolutionCtrl;

	// Condition values
	int attackedTimes;
	int killedUnits;
	int damageCaused;
	int damageIncreased;

	// Constructor
	public Champion(String name, int cost, int attack, int maxHealth, LevelUpConditions condition, int conditionParameter, EvolutionController evolutionCtrl, Board board) {
		super(name, cost, attack, maxHealth, board);
		this.hasLeveledUp = false;
		this.condition = condition;
		this.evolutionCtrl = evolutionCtrl;
		this.conditionParameter = conditionParameter;
		attackedTimes = 0;
		killedUnits = 0;
		damageCaused = 0;
		damageIncreased = 0;
	}

	// Methods
	private void evolve() {
		if (!this.hasLeveledUp) {
			evolutionCtrl.evolve(this);
			this.hasLeveledUp = true;
		}
	}

	public void addAttack(int value) {
		super.addAttack(value);
		damageIncreased += value;
		if (condition == LevelUpConditions.INCR_N_DAMAGE && damageIncreased >= conditionParameter) {
			evolve();
		}
	}

	public void attack(Combatant other) {
		super.attack(other);
		attackedTimes += 1;
		if (condition == LevelUpConditions.ATTACK_N_TIMES && attackedTimes >= conditionParameter) {
			evolve();
		}

		if (other.isDead()) {
			killedUnits += 1;
		}

		if (condition == LevelUpConditions.KILL_N_UNITS && killedUnits >= conditionParameter) {
			evolve();
		}

		damageCaused += getDamage();
		if (condition == LevelUpConditions.CAUSE_N_DAMAGE && damageCaused >= conditionParameter) {
			evolve();
		}
	}

	@Override
	public String getLongDescription() {
		// Super from Unit.
		String description = super.getLongDescription();

		// Level up condition.
		description += " Level up: " + this.condition.getDescription(this.conditionParameter) + ".";

		// Level up upgrade.
		description += this.evolutionCtrl.getLongDescription();

		return description;
	}

}
