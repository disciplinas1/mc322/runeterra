package com.unicamp.mc322.runeterra.card;

public enum Trait {
	DOUBLE_ATTACK("Double attack"),
	ELUSIVE("Elusive"),
	RAGE("Rage");

	String label;

	private Trait(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return this.label;
	}
}
