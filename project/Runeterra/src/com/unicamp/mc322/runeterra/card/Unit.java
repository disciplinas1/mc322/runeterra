package com.unicamp.mc322.runeterra.card;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.unicamp.mc322.runeterra.effects.Effect;
import com.unicamp.mc322.runeterra.game.*;
import com.unicamp.mc322.runeterra.utils.PairContainer;

public class Unit extends Card implements Combatant {

	// Properties
	protected int permanentAttack;
	private int temporaryAttack;
	protected int maxHealth;
	private int currentHealth;
	protected ArrayList<Trait> traits;
	private int barrier;
	private Map<UnitEffectTrigger, ArrayList<Effect>> effectsMap;
	private int attacksThisRound;
	private int rageAttack;
	private int rageHealth;

    // Constructor
    public Unit(String name, int manaCost, int attack, int maxHealth, Board board) {
        super(name, manaCost, board);
		this.permanentAttack = attack;
		this.temporaryAttack = attack;
		this.maxHealth = maxHealth;
		this.currentHealth = maxHealth;
		this.traits = new ArrayList<Trait>();
		this.effectsMap = new HashMap<UnitEffectTrigger, ArrayList<Effect>>();
		this.barrier = 0;
		this.attacksThisRound = 0;
		this.rageHealth = 0;
		this.rageAttack = 0;
    }

    public void addEffect(UnitEffectTrigger trigger, Effect effect) {
    	ArrayList<Effect> effects = this.effectsMap.get(trigger);
    	if (effects == null) {
    		effects = new ArrayList<Effect>();
    		this.effectsMap.put(trigger, effects);
    	}

    	effects.add(effect);
    }

    public void addEffects(Iterable<PairContainer<Effect, UnitEffectTrigger>> effectPair) {
		if (effectPair == null) {
			return;
		}

		for (PairContainer<Effect, UnitEffectTrigger> pair: effectPair) {
			addEffect(pair.getSecond(), pair.getFirst());
		}
    }

    private void callEffects(UnitEffectTrigger trigger) {
    	ArrayList<Effect> effects = this.effectsMap.get(trigger);
    	if (effects == null) {
    		return;
    	}

    	for (Effect effect: effects) {
    		effect.applyEffect(board, this);
    	}
    }

    public void addTemporaryAttack(int value) {
    	temporaryAttack += value;
    }

    // Unit will have this attack until next round
    public void setTemporaryAttack(int value) {
    	temporaryAttack = value;
    }

    public void addBarrier(int value) {
    	if (value <= 0) {
    		return;
    	}

    	barrier += value;
    }

    public void addBarrier() {
    	addBarrier(1);
    }

	@Override
	public void onCardDraw(Player player) {
		super.onCardDraw(player);
		this.activateEffectOnCardDraw();
	}

    public void activateEffectOnRoundEnd() {
    	barrier = 0;
    	temporaryAttack = permanentAttack;
    	this.callEffects(UnitEffectTrigger.END_ROUND);
    	attacksThisRound = 0;
    }

    private void activateEffectOnCardDraw() {
    	this.callEffects(UnitEffectTrigger.ON_DRAW);
    }

    private void activateEffectOnDeath() {
    	this.callEffects(UnitEffectTrigger.ON_DEATH);
    }

    public boolean hasTrait(Trait trait) {
    	return this.traits.contains(trait);
    }

    public void addTrait(Trait trait) {
    	if (!hasTrait(trait)) {
    		traits.add(trait);
    	}
    }

    public void setRageParams(int attack, int health) {
    	this.rageAttack = attack;
    	this.rageHealth = health;
    }

    public void addTraits(Iterable<Trait> traits) {
    	for (Trait trait: traits) {
    		addTrait(trait);
    	}
    }

	@Override
    public void attack(Combatant other) {
    	this.attackOnce(other);
		if (this.hasTrait(Trait.DOUBLE_ATTACK) && !this.isDead() && !other.isDead()) {
			this.attackOnce(other);
		}

    	if (this.hasTrait(Trait.RAGE) && other.isDead()) {
			this.permanentAttack += this.rageAttack;
			this.temporaryAttack += this.rageAttack;
    		this.maxHealth += this.rageHealth;
    	}
    }

	private void attackOnce(Combatant other) {
		other.takeDamage(this.getDamage());
    	if (other.isDead()) {
			this.callEffects(UnitEffectTrigger.ON_ATTACK_DESTROID_ENEMY);
    	}

		this.attacksThisRound += 1;
	}

    public int getDamage() {
        return temporaryAttack;
    }

    public int getPermanentAttack() {
    	return this.permanentAttack;
    }

    @Override
    public void takeDamage(int value) {
    	if (barrier > 0) {
    		barrier -= 1;
    	} else {
    		this.currentHealth -= value;
    	}

    	if (this.isDead()) {
    		this.activateEffectOnDeath();
    	}
    }

	@Override
	public boolean isDead() {
		return this.currentHealth <= 0;
	}

	public void addAttack(int value) {
		this.permanentAttack += value;
	}

	public void addHealth(int value) {
		this.maxHealth += value;
		this.currentHealth += value;
	}

	public int getMaxHealth() {
		return this.maxHealth;
	}

	public int getCurrentHealth() {
		return this.currentHealth;
	}

	public void heal(int value) {
		this.currentHealth = Math.min(this.currentHealth + value, this.maxHealth);
	}

	@Override
	public void play(Player player) {
		player.spendMana(manaCost);
		PlayerBoard ownBoard = board.getPlayerBoard(player);
		ownBoard.addToEvokedUnits(this);
		this.callEffects(UnitEffectTrigger.ON_EVOKED);
	}

	@Override
	public boolean canBePlayed(Player player) {
		int evokedUnits = board.getPlayerBoard(this.owner).getEvokedUnits().size();
		return player.getMana() >= manaCost && evokedUnits < PlayerBoard.maxEvokedUnits;
	}

	public boolean canAttack() {
		if (this.hasTrait(Trait.DOUBLE_ATTACK)) {
			return this.attacksThisRound <= 1;
		} else {
			return this.attacksThisRound <= 0;
		}
	}

	public boolean canBlock(Unit other) {
		if (other.hasTrait(Trait.ELUSIVE)) {
			return this.hasTrait(Trait.ELUSIVE);
		} else {
			return true;
		}
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public String getLongDescription() {
		// Name and mana.
		String description = super.getLongDescription();

		// Attack and health.
		description += " " + this.temporaryAttack + "/" + this.currentHealth + ".";

		// Traits.
		for (Trait trait: this.traits) {
			description += " " + trait + ".";
		}

		// Triggers and effects.
		description += getEffectString();

		if (this.barrier > 0) {
			description += " Has a barrier of " + this.barrier + ".";
		}

		return description;
	}

	private String getEffectString() {
		String description = "";

		for (Map.Entry<UnitEffectTrigger, ArrayList<Effect>> entry: effectsMap.entrySet()) {
			description += " " + entry.getKey() + ":";
			ArrayList<Effect> effects = entry.getValue();
			for (Effect effect: effects) {
				description += " " + effect.getLongDescription();

				if (effects.indexOf(effect) != effects.size() - 1) {
					description += ";";
				} else {
					description += ".";
				}
			}
		}

		return description;
	}

}
