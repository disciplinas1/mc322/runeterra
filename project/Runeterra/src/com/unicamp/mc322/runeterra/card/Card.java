package com.unicamp.mc322.runeterra.card;

import java.util.ArrayList;
import com.unicamp.mc322.runeterra.game.*;

public abstract class Card implements Describable {

	// Properties
    protected String name;
    protected int manaCost;
	protected Board board;
	protected Player owner;

    // Constructor
    public Card(String name, int cost, Board board) {
        this.name = name;
        this.manaCost = cost;
		this.board = board;
    }

	// Methods
	abstract public boolean canBePlayed(Player player);

	abstract public void play(Player player);

    public String getName() {
    	return name;
    }

	public ArrayList<Unit> getEnemyUnits() {
		return this.board.getPlayerEnemyUnits(owner);
	}

	public ArrayList<Unit> getAlliedUnits() {
		return this.board.getPlayerEvokedUnits(owner);
	}

	public Player getEnemyPlayer() {
		return this.board.getOtherPlayer(owner);
	}

	public Player getPlayer() {
		return owner;
	}

	public void onCardDraw(Player player) {
		this.owner = player;
	}

	public String getLongDescription() {
		return this.name + ". Mana cost: " + this.manaCost + ".";
	}

	@Override
	public String toString() {
		return this.name;
	}

}
