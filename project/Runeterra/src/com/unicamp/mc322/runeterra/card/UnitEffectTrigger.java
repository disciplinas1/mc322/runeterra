package com.unicamp.mc322.runeterra.card;

public enum UnitEffectTrigger {
	END_ROUND("At the end of the round"),
	ON_DRAW("When drawn"),
	ON_EVOKED("When evoked"),
	ON_ATTACK_DESTROID_ENEMY("When destroying an enemy unit on attacking"),
	ON_DEATH("At death");

	String label;

	private UnitEffectTrigger(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return this.label;
	}

}
