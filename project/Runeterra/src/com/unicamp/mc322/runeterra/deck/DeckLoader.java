package com.unicamp.mc322.runeterra.deck;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;

import com.github.cliftonlabs.json_simple.*;
import com.unicamp.mc322.runeterra.card.*;
import com.unicamp.mc322.runeterra.effects.*;
import com.unicamp.mc322.runeterra.game.Board;
import com.unicamp.mc322.runeterra.utils.PairContainer;

public class DeckLoader {

	// Static Properties
	private static String sep = File.separator;
	private static String decksDirPath = sep + "resources" + sep + "decks" + sep;
	private static String allCardsRelativePath = sep + "resources" + sep + "cards.json";

	// Properties
	private Map<String, JsonObject> allCardsJson;
	private static DeckLoader self = null;
	private Board board;

	// Private Singleton Constructor
	private DeckLoader() {
		allCardsJson = new HashMap<String, JsonObject>();
		this.loadCards();
	}

	// Methods
	public void setBoard(Board board) {
		this.board = board;
	}

	public static DeckLoader getInstance() {
		if (self == null) {
			self = new DeckLoader();
		}

		return self;
	}

	public Deck getDeck(String deckFileName) {
		Deck deck = new Deck();
		String deckPath = (System.getProperty("user.dir") + decksDirPath + deckFileName + ".json");

		try (FileReader fileReader = new FileReader(deckPath)) {
			JsonObject jsonDeck = (JsonObject) Jsoner.deserialize(fileReader);
			JsonArray jsonCards = (JsonArray) jsonDeck.get("cards");
			for (Object objCard: jsonCards) {
				JsonObject jsonCard = (JsonObject) objCard;
				String cardName = (String) jsonCard.get("name");
				int cardQuantity = getIntegerProperty(jsonCard, "quantity");
				for (int i = 0; i < cardQuantity; i++) {
					deck.addCard(getCard(cardName));
				}
			}

			return deck;

		} catch (FileNotFoundException e) {
			System.out.format("1\n");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.format("2\n");
			e.printStackTrace();
		} catch (JsonException e) {
			System.out.format("3\n");
			e.printStackTrace();
		}

		return deck;
	}

	public Card getCard(String name) {
		JsonObject jsonCard = this.allCardsJson.get(name);
		if (jsonCard == null) {
			throw new IllegalArgumentException("No such card in resources: " + name);
		}

		return this.parseCard(jsonCard);
	}

	private UnitEffectTrigger parseEffectTrigger(String string) {
		switch (string) {
			case "end_round":
				return UnitEffectTrigger.END_ROUND;
			case "destroy_enemy":
				return UnitEffectTrigger.ON_ATTACK_DESTROID_ENEMY;
			case "death":
				return UnitEffectTrigger.ON_DEATH;
			case "draw":
				return UnitEffectTrigger.ON_DRAW;
			case "evoked":
				return UnitEffectTrigger.ON_EVOKED;
			default:
				return null;
		}
	}

	private PossibleUnitTargetType parseUnitTarget(String string) {
		switch (string) {
			case "own_units":
				return PossibleUnitTargetType.ALL_OWN_UNITS;
			case "enemy_units":
				return PossibleUnitTargetType.ALL_ENEMY_UNITS;
			case "chosen":
				return PossibleUnitTargetType.CHOSEN;
			case "self":
				return PossibleUnitTargetType.SELF;
			default:
				return null;
		}
	}

	private PossiblePlayerTarget parsePlayerTarget(String string) {
		switch (string) {
			case "self":
				return PossiblePlayerTarget.SELF;
			default:
				return PossiblePlayerTarget.ENEMY;
		}
	}

	private Iterable<PairContainer< Effect, UnitEffectTrigger>> parseEffects(JsonArray json) {
		ArrayList<PairContainer<Effect, UnitEffectTrigger>> result = new ArrayList<PairContainer<Effect, UnitEffectTrigger >>();
		if (json == null) {
			return result;
		}

		for (Object obj: json) {
			JsonObject jsonObj = (JsonObject) obj;
			Effect effect = parseEffect(jsonObj);
			UnitEffectTrigger trigger = this.parseEffectTrigger((String) jsonObj.get("trigger"));
			result.add(new PairContainer<Effect, UnitEffectTrigger>(effect, trigger));
		}

		return result;
	}

	private Effect parseEffect(JsonObject json) {
		Effect result = null;
		PossibleUnitTargetType targetUnit;
		PossiblePlayerTarget targetPlayer;
		int param = 0;
		String paramString;
		String effectString = (String) json.get("effect");
		String targetString = (String) json.get("target");
		switch (effectString) {
			case "full_heal":
				targetUnit = this.parseUnitTarget(targetString);
				result = new FullHeal(targetUnit);
				break;
			case "add_n_m":
				targetUnit = this.parseUnitTarget(targetString);
				JsonObject addNMParamsJson = (JsonObject) json.get("param");
				int paramN = getIntegerProperty(addNMParamsJson, "n");
				int paramM = getIntegerProperty(addNMParamsJson, "m");
				result = new ModifyUnitAttackAndHealth(targetUnit, paramN, paramM);
				break;
			case "draw_card":
				targetPlayer = this.parsePlayerTarget(targetString);
				result = new DrawCard(targetPlayer);
				break;
			case "doubles_attack_health":
				targetUnit = this.parseUnitTarget(targetString);
				result = new DoubleAttackAndHealth(targetUnit);
				break;
			case "combat":
				targetUnit = this.parseUnitTarget(targetString);
				result = new UnitCombatEnemyUnit(targetUnit);
				break;
			case "unit_attack_enemy_nexus":
				targetUnit = this.parseUnitTarget(targetString);
				result = new UnitAttackEnemyNexus(targetUnit);
				break;
			case "unit_attack_all_enemy_units":
				targetUnit = this.parseUnitTarget(targetString);
				result = new UnitAttackAllEnemyUnits(targetUnit);
				break;
			case "set_attack_unitl_next_turn":
				targetUnit = this.parseUnitTarget(targetString);
				param = getIntegerProperty(json, "param");
				result = new SetTemporaryAttack(targetUnit, param);
				break;
			case "give_barrier":
				targetUnit = this.parseUnitTarget(targetString);
				param = (int) json.getOrDefault("param", 1);
				result = new GiveBarrier(targetUnit, param);
				break;
			case "damage_nexus":
				targetPlayer = this.parsePlayerTarget(targetString);
				param = getIntegerProperty(json, "param");
				result = new DamagePlayer(targetPlayer , param);
				break;
			case "add_card_to_hand":
				targetPlayer = this.parsePlayerTarget(targetString);
				paramString = (String) json.get("param");
				result = new AddCardPlayerHand(targetPlayer, paramString);
				break;
			default:
				//todo : jogar erro
				break;
		}

		return result;
	}

	private Iterable<Trait> parseTraits(JsonArray json) {
		ArrayList<Trait> traits = new ArrayList<Trait>();
		if (json == null) {
			return null;
		}

		for (Object traitObj : json) {
			String traitString = (String) traitObj;
			switch (traitString) {
			case "rage":
				traits.add(Trait.RAGE);
				break;
			case "elusive":
				traits.add(Trait.ELUSIVE);
				break;
			case "double_attack":
				traits.add(Trait.DOUBLE_ATTACK);
				break;
			}
		}

		return traits;
	}

	private LevelUpConditions parseLevelUpCondition(String condition) {
		switch (condition) {
			case "attack_n_times":
				return LevelUpConditions.ATTACK_N_TIMES;
			case "kill_n_units" :
				return LevelUpConditions.KILL_N_UNITS;
			case "cause_n_damage":
				return LevelUpConditions.CAUSE_N_DAMAGE;
			case "INCR_N_DAMAGE":
				return LevelUpConditions.INCR_N_DAMAGE;
		}

		return null;
	}

	private EvolutionController parseEvolutionCtr(JsonObject json) {
		int attack = getIntegerProperty(json, "attack");
		int health = getIntegerProperty(json, "health");
		Iterable<PairContainer<Effect, UnitEffectTrigger>> effects = parseEffects((JsonArray) json.get("effects"));
		Iterable<Trait> traits = this.parseTraits((JsonArray) json.get("traits"));
		EvolutionController evolCtr = new EvolutionController(attack , health);
		for (PairContainer<Effect, UnitEffectTrigger> pair: effects) {
			evolCtr.addEffect(pair.getSecond(), pair.getFirst());
		}

		evolCtr.addTraits(traits);
		return evolCtr;
	}

	private PairContainer<Integer, Integer> parseRageParams(JsonObject json) {
		if (json == null) {
			return null;
		}

		int attack = getIntegerProperty(json, "attack");
		int health = getIntegerProperty(json, "health");
		return new PairContainer<Integer, Integer>(attack, health);
	}

	private Card parseCard(JsonObject jsonCard) {
		// All cards properties
		String cardType = (String) jsonCard.get("type");
		String cardName = (String) jsonCard.get("name");
		int cardCost = getIntegerProperty(jsonCard, "cost");
		JsonArray effectsJson = (JsonArray) jsonCard.get("effects");
		Iterable<PairContainer<Effect, UnitEffectTrigger>> effects = this.parseEffects(effectsJson);

		// Spell case
		if (cardType.compareTo("spell") == 0) {
			Spell spell = new Spell(cardName, cardCost, board);
			for (PairContainer<Effect, UnitEffectTrigger> effectPair: effects) {
				spell.addEffect(effectPair.getFirst());
			}

			return spell;
		}

		// Unit properties
		int cardHealth = getIntegerProperty(jsonCard, "health");
		int cardAttack = getIntegerProperty(jsonCard, "attack");
		JsonArray traitsJson = (JsonArray) jsonCard.get("traits");
		Iterable<Trait> traits= parseTraits(traitsJson);
		PairContainer<Integer, Integer> rageParams = this.parseRageParams((JsonObject) jsonCard.get("rage_param"));

		// Unit and champion cases
		switch (cardType) {
			case "unit":
				Unit unit = new Unit(cardName, cardCost, cardAttack, cardHealth, board);
				unit.addTraits(traits);
				unit.addEffects(effects);
				if (unit.hasTrait(Trait.RAGE)) {
					unit.setRageParams(rageParams.getFirst(), rageParams.getSecond());
				}

				return unit;

			case "champion":
				JsonObject evolutionJson = (JsonObject) jsonCard.get("evolution");
				LevelUpConditions levelUpCondition = parseLevelUpCondition((String) evolutionJson.get("condition"));
				int conditionParam = getIntegerProperty(evolutionJson, "condition_param");
				EvolutionController evlCtrl = this.parseEvolutionCtr(evolutionJson);
				Champion champion = new Champion(cardName, cardCost, cardAttack, cardHealth, levelUpCondition, conditionParam, evlCtrl, board);
				champion.addTraits(traits);
				champion.addEffects(effects);
				if (champion.hasTrait(Trait.RAGE)) {
					champion.setRageParams(rageParams.getFirst(), rageParams.getSecond());
				}

				return champion;

			default:
				throw new IllegalStateException("Undefined card type: " + cardType);
		}
	}

	private void loadCards() {
		String allCardsPath = (System.getProperty("user.dir") + allCardsRelativePath);
		try (FileReader fileReader = new FileReader(allCardsPath) ) {

			JsonArray jsonCards = (JsonArray) Jsoner.deserialize(fileReader);
			for (Object objCard: jsonCards) {
				JsonObject jsonCard = (JsonObject) objCard;
				String name = (String) jsonCard.get("name");
				allCardsJson.put(name, jsonCard);
			}

		} catch (FileNotFoundException e) {
			System.out.format("1\n");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.format("2\n");
			e.printStackTrace();
		} catch (JsonException e) {
			System.out.format("3\n");
			e.printStackTrace();
		}
	}

	private int getIntegerProperty(JsonObject jsonObject, String property) {
		return ((BigDecimal) jsonObject.get(property)).intValue();
	}

}
