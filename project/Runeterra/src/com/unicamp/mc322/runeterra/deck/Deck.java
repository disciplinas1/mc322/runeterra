package com.unicamp.mc322.runeterra.deck;

import java.util.ArrayList;
import java.util.Random;

import com.unicamp.mc322.runeterra.card.Card;
import com.unicamp.mc322.runeterra.game.Player;

public class Deck {

	private ArrayList<Card> cards;
    private Random random;

    // Constructor
    public Deck() {
    	cards = new ArrayList<Card>();
        random = new Random();
    }

    // Methods
    public Card drawCard(Player player) {
        Card card = cards.remove(random.nextInt(cards.size()));
        card.onCardDraw(player);
        return card;
    }

    public void addCard(Card card) {
    	cards.add(card);
    }

    public int getSize() {
        return cards.size();
    }

}
