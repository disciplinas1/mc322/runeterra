package com.unicamp.mc322.runeterra;

import com.unicamp.mc322.runeterra.game.Game;

public class Runner {

	public static void main(String[] args) {
		Game game = new Game();
		game.play();
	}

}
